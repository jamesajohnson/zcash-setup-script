# Welcome

Very simple Zcash Mining set up script. The script will automatically clone, build and configure the Zcash source and start mining.

By default CPU mining and the alternate, more efficient, miner are enabled.

## To Use:

```
 $ git clone https://jamesajohnson@bitbucket.org/jamesajohnson/zcash-setup-script.git/wiki
```
1. Use the above to clone the script, or download the source manually.
2. Change USERNAME in the script to your user name.
3. sudo ./mine.sh

## About:
The script does nothing special. It's based on the instruction from the [Zcash Wiki](https://github.com/zcash/zcash/wiki/1.0-User-Guide). It's just an easier way to get started mining.

##Requirements
See the [Zcash Wiki Page](https://github.com/zcash/zcash/wiki/1.0-User-Guide)